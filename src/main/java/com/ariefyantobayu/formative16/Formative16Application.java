package com.ariefyantobayu.formative16;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ariefyantobayu.controller.ArisanController;

@SpringBootApplication
public class Formative16Application {	
	public static void main(String[] args) {
		SpringApplication.run(ArisanController.class, args);	
	}

}
