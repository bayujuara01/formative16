# formative16

## Nexsoft Fun Coding Bootcamp Day 16 Formative

## Author
### Bayu Seno Ariefyanto

## Running Program
```

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v2.5.5)
================================
=====APLIKASI ARISAN SIMPLE=====
================================
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 1
Masukkan Nama : Bayu
Masukkan Alamat : Purworejo
[Member Berhasil Ditambah]
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 1
Masukkan Nama : Anies
Masukkan Alamat : Semarang
[Member Berhasil Ditambah]
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : Akbar
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 1
Masukkan Nama : Akbar
Masukkan Alamat : Surabaya
[Member Berhasil Ditambah]
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 3
==============INFO==============
Arisan Belum Dibuka, Silahkan
tambah anggota & pilih 1
untuk inisialisasi
================================
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 4
===============INFO=============
      Arisan Belum Dimulai
================================
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 2
==============INFO==============
Arisan Telah Dibuka,
Pada : 2021-10-21
================================
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 4
===============INFO=============
Tanggal Mulai : 2021-10-21
Basar Iuran : Rp. 150000
Anggota : 
Member {name: Bayu, code: none, address: Purworejo}
Member {name: Anies, code: none, address: Semarang}
Member {name: Akbar, code: none, address: Surabaya}
================================
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 3
================================
============PEMENANG============
Selamat Bayu, dengan kode 4o5Pl8jJJ53R,
mendapat : Rp. 450000
================================
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 3
================================
============PEMENANG============
Selamat Anies, dengan kode M1JsLxrNU5pV,
mendapat : Rp. 450000
================================
1. Tambah Anggota
2. Inisialisasi Arisan
3. Kocok
4. Info
0. Keluar
Pilih : 3
================================
======ARISAN TELAH SELESAI======
================================
```
